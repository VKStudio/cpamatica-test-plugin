<?php

function get_post_id_by_title(string $title = ''): int
{

    $posts = get_posts(array(
            'post_type'              => 'post',
            'title'                  => $title,
            'numberposts'            => 1,
            'update_post_term_cache' => false,
            'update_post_meta_cache' => false,
            'orderby'                => 'post_date ID',
            'order'                  => 'ASC',
            'fields'                 => 'ids'
        ));
    return empty($posts) ? get_the_ID() : $posts[0];
}

function wp_post_exists($id): bool
{

    return is_string(get_post_status($id));
}
