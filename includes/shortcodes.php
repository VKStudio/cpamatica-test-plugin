<?php

$shortcode_used = false;
function articles_shortcode_function($atts): string
{
    global $shortcode_used;
    $shortcode_used = true;
    $atts = shortcode_atts(array(
            'title' => '',
            'count' => 5,
            'sort'  => 'date',
            'ids'   => '',
        ), $atts, 'articles_shortcode');
    $title_section = esc_html($atts['title']);
    $count = intval($atts['count']);
    $sort  = esc_attr($atts['sort']);
    $ids   = esc_attr($atts['ids']);
    if (!empty($ids)) {
        $post_ids = array_map('intval', explode(',', $ids));
        $posts = get_posts(array(
            'post_type' => 'post',
            'post__in'  => $post_ids,
            'orderby'   => $sort,
            'numberposts' => $count,
        ));
    } else {
        $orderby_map = array(
            'date'   => 'date',
            'title'  => 'title',
            'rating' => 'meta_value',
        );
        $posts = get_posts(array(
            'post_type'   => 'post',
            'orderby'     => $orderby_map[ $sort ] ?? 'date',
            'meta_key'    => ($sort === 'rating') ? 'rating' : '',
            'numberposts' => $count,
        ));
    }

    ob_start();
    include PLUGIN_TEMPLATE_PATH . 'templates/posts.php';
    wp_reset_postdata();
    return ob_get_clean();
}

add_shortcode('articles_shortcode', 'articles_shortcode_function');


function enqueue_custom_styles(): void
{
    global $shortcode_used;
    if ($shortcode_used) {
        wp_enqueue_style('main-style', PLUGIN_TEMPLATE_URI . 'css/style.css');
    }
}

add_action('wp_enqueue_scripts', 'enqueue_custom_styles');
