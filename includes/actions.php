<?php

use CpamaticaPlugin\APIPostFetcher;
use CpamaticaPlugin\WPCategoryManager;
use CpamaticaPlugin\WPPostCreator;
use CpamaticaPlugin\WPPrepareData;
use CpamaticaPlugin\WPPrepareThumbnail;

add_action('admin_menu', 'test_plugin_admin_page');

function test_plugin_admin_page(): void
{
    add_menu_page(
        'Cpamatica',
        'Cpamatica',
        'manage_options',
        'cpamatica-plugin',
        'cpamatica_plugin_main_page',
        'dashicons-schedule',
        20
    );
}

function cpamatica_plugin_main_page(): void
{
    include PLUGIN_TEMPLATE_PATH . 'templates/main-page.php';
}

// Cron actions
if (!wp_next_scheduled('cpamatica_daily_event')) {
    wp_schedule_event(time(), 'daily', 'cpamatica_daily_event');
}

add_action('cpamatica_daily_event', 'cpamatica_get_posts_callback');

function cpamatica_get_posts_callback(): void
{
    $post_fetcher = new APIPostFetcher(API_URL, API_KEY);
    $posts = $post_fetcher->fetchPosts();
    $categoryManager = new WPCategoryManager();

    foreach ($posts as $data_post) {
        try {
            $prepared_data = new WPPrepareData($data_post, $categoryManager);
            $prepared_data = $prepared_data->prepareArgsForPost();

            $new_post = new WPPostCreator($prepared_data);
            $post_id = $new_post->insertPost();

            $media = new WPPrepareThumbnail($post_id, $data_post->image);
            $media->setFeaturedImage();
        } catch (Exception $e) {
            error_log($e->getMessage());
            continue;
        }
    }
}


// Scripts and styles
function register_cpamatica_plugin_scripts(): void
{
    wp_register_script('cpamatica-plugin', plugins_url('cpamatica/js/script.js'));
}

add_action('admin_enqueue_scripts', 'register_cpamatica_plugin_scripts');

function load_test_plugin_scripts($hook): void
{
    if ($hook != 'toplevel_page_cpamatica-plugin') {
        return;
    }

    wp_enqueue_script('cpamatica-plugin');
    wp_enqueue_style('main-style', PLUGIN_TEMPLATE_URI . 'css/style.css');
}

add_action('admin_enqueue_scripts', 'load_test_plugin_scripts');
