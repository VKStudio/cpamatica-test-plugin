$ = jQuery.noConflict();
$(document).ready(function () {
    $('#get_posts').on('click', function (e) {
        e.preventDefault();
        const alert = $('.output-alert');
        const output = $('.output');

        const data = {
            action: 'cpamatica_get_posts',
        }

        jQuery.get(ajaxurl, data, function (response) {
            console.log(response)
        });
    });
});