<div class="wrap">
    <h3>
        Cpamatica Test Plugin
    </h3>
    <br>
    <h3>For display post please use shortcode:</h3>
    <code>[articles_shortcode title="Your title" count=3 sort="title" ids="1,5,14"]</code>
    <br>
    <br>
    <code>title=""</code> - Your title for section
    <br>
    <code>count=</code> - Number of posts for display
    <br>
    <code>sort=""</code> - Sort posts by <i>title</i> or <i>date</i> of <i>rating</i>
    <br>
    <code>ids=""</code> - Ids of posts (not mandatory)
    <br>
</div>