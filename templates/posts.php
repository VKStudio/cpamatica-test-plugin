<div class="articles">
    <h2><?= $title_section ?></h2>
    <div class="article_cards">
        <?php
        global $post;
        foreach ($posts as $post) :
            setup_postdata($post);

            $post_title = get_the_title();
            $post_permalink = get_permalink();
            $post_category = get_the_category()[0];
            $post_category_name = $post_category->name;
            $post_category_link = get_category_link($post_category);
            $post_thumbnail = get_the_post_thumbnail_url();
            $post_rating = get_post_meta($post->ID, 'rating', true);
            $post_site_link = get_post_meta($post->ID, 'site_link', true);

            $shortened_title = mb_strimwidth($post_title, 0, 80, '...');
            ?>
            <div class="card">
                <div class="card_image">
                    <img src="<?= $post_thumbnail ?>" alt="post thumbnail">
                </div>
                <div class="card_info">
                    <div class="category"><?= $post_category_name ?></div>
                    <div class="title"><?= $shortened_title ?></div>
                    <div class="actions">
                        <a href="<?= $post_permalink ?>" class="read_more">Read More</a>
                        <div class="additional">
                            <?php if ($post_rating) : ?>
                                <div class="rating">⭐ <?= $post_rating ?></div>
                            <?php endif; ?>
                            <?php if ($post_site_link) : ?>
                                <a href="<?= $post_site_link ?>" rel="nofollow" class="visit_btn">Visit Site</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach;
        wp_reset_postdata();
        ?>
    </div>
</div>
