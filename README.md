# cpamatica-test-plugin



## About Plugin

What this plugin does:
1. Creates a wp cron task that runs once a day.
2. The task is to obtain content through the API from which plugin create WordPress new posts.
3. The response from the API has the following information: Title, Content, Link to image, link to external resource, rating.
4. Using a shortcode to display posts with different possible parameters.
For display posts use shortcode

```
[articles_shortcode title="Your title" count=3 sort="title" ids="1,5,14"]

title="" - Your title for section
count= - Number of posts for display
sort="" - Sort posts by title or date of rating
ids="" - Ids of posts (not mandatory) 
```