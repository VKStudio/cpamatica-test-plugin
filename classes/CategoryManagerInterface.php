<?php

namespace CpamaticaPlugin;

interface CategoryManagerInterface
{
    public function getCategoryByName($category_name): int;
}