<?php

namespace CpamaticaPlugin;

class WPPrepareData
{
    protected object $post_data;
    protected array $post_args;
    public function __construct($post_data, protected CategoryManagerInterface $categoryManager)
    {
        $this->post_data = $post_data;
    }

    public function prepareArgsForPost(): array
    {
        $this->post_args = [
            'post_author' => $this->getAdminId(),
            'post_content' => $this->post_data->content,
            'post_date' => $this->generateRandomDate(),
            'post_status' => 'publish',
            'post_title' => $this->post_data->title,
            'post_type' => 'post',
            'meta_input' => [
                'rating' => $this->post_data->rating,
                'site_link' => $this->post_data->site_link,
            ],
        ];

        if ($this->post_data->category) {
            $this->post_args['post_category'] = [$this->categoryManager->getCategoryByName($this->post_data->category)];
        }

        return $this->post_args;
    }

    private function getAdminId(): int
    {
        $admin_users = get_users(array(
            'role'    => 'administrator',
            'number'  => 1,
        ));

        if (empty($admin_users)) {
            throw new \InvalidArgumentException("NO ADMINS FOUND!");
        }

        return array_shift($admin_users)->ID;
    }

    private function generateRandomDate(): string
    {
        $current_time = current_time('timestamp');
        $one_month_ago = strtotime('-1 month', $current_time);
        $random_timestamp = mt_rand($one_month_ago, $current_time);

        return date('Y-m-d H:i:s', $random_timestamp);
    }

    /**
     * @return array
     */
    public function getPostArgs(): array
    {
        return $this->post_args;
    }
}
