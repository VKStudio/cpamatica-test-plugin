<?php

namespace CpamaticaPlugin;

use Exception;
use InvalidArgumentException;

class WPPrepareThumbnail
{
    public string $image_url;

    /**
     * @throws Exception
     */
    public function __construct(protected int $post_id, $image_url)
    {
        if (!$image_url) {
            throw new InvalidArgumentException("Invalid Image URL");
        }

        $this->image_url = $image_url;
    }

    public function setFeaturedImage(): void
    {
        $media_id = media_sideload_image($this->image_url, $this->post_id, '', 'id');

        if (!is_wp_error($media_id)) {
            set_post_thumbnail($this->post_id, $media_id);
        }
    }
}
