<?php

namespace CpamaticaPlugin;

use Exception;

class WPPostCreator
{
    public function __construct(protected array $post_args)
    {
    }

    /**
     * @throws Exception
     */
    public function insertPost(): int
    {
        $title = $this->post_args['post_title'];

        if (get_post_id_by_title($title)) {
            throw new Exception("Post $title exists.");
        }

        if (!$post_id = $this->createPost($this->post_args)) {
            throw new Exception('Error while creating post!');
        };

        return $post_id;
    }

    private function createPost($post_data): int
    {
        return wp_insert_post($post_data);
    }
}
