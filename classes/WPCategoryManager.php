<?php

namespace CpamaticaPlugin;

class WPCategoryManager implements CategoryManagerInterface
{
    public function getCategoryByName($category_name): int
    {
        $category_id = $this->getCategoryIdByName($category_name);

        if (!$category_id) {
            $category_id = $this->createCategory($category_name);
        }

        return $category_id;
    }

    private function getCategoryIdByName($category_name): bool|int
    {
        $category = get_term_by('name', $category_name, 'category');

        return $category ? $category->term_id : false;
    }

    private function createCategory($category_name): int
    {
        return wp_create_category($category_name);
    }
}
