<?php

namespace CpamaticaPlugin;

class APIPostFetcher
{

    public function __construct(private string $api_url, private string $api_key)
    {
    }

    public function fetchPosts(): array
    {
        $response = wp_remote_get($this->api_url, array(
            'headers' => array(
                'X-API-Key' => $this->api_key,
            ),
        ));

        if (is_wp_error($response)) {
            return [];
        }

        $data = wp_remote_retrieve_body($response);

        return json_decode($data);
    }
}
