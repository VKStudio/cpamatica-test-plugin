<?php

/*
* Plugin Name: Test Plugin for Cpamatica
* Description: Test Plugin for Cpamatica
* Version: 1.0
* Author: Viktor Konopelko
* Author URI: https://codex.wordpress.org/
*/

define('PLUGIN_TEMPLATE_PATH', plugin_dir_path(__FILE__));
define('PLUGIN_TEMPLATE_URI', plugin_dir_url(__FILE__));
define('API_URL', 'https://my.api.mockaroo.com/posts.json');
define('API_KEY', '413dfbf0');

require_once ABSPATH . '/wp-admin/includes/media.php';
require_once ABSPATH . '/wp-admin/includes/file.php';
require_once ABSPATH . '/wp-admin/includes/image.php';
require_once ABSPATH . '/wp-admin/includes/taxonomy.php';

include PLUGIN_TEMPLATE_PATH . 'classes/CategoryManagerInterface.php';

include PLUGIN_TEMPLATE_PATH . 'classes/APIPostFetcher.php';
include PLUGIN_TEMPLATE_PATH . 'classes/WPPostCreator.php';
include PLUGIN_TEMPLATE_PATH . 'classes/WPPrepareData.php';
include PLUGIN_TEMPLATE_PATH . 'classes/WPPrepareThumbnail.php';
include PLUGIN_TEMPLATE_PATH . 'classes/WPCategoryManager.php';

include PLUGIN_TEMPLATE_PATH . 'includes/actions.php';
include PLUGIN_TEMPLATE_PATH . 'includes/functions.php';
include PLUGIN_TEMPLATE_PATH . 'includes/shortcodes.php';
